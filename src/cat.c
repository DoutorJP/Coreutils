#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  char ch;
  FILE * fp = fopen(argv[1], "r");
  if(!(argv[1])) {
    puts("Please insert an file");
    return 1;
  }
  do {
	ch = fgetc(fp);
	printf("%c", ch);
  }
  while(ch != EOF);
  fclose(fp);
  return 0;
}
