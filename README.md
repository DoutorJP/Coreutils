# Coreutils
A simple replacement for (some of) GNU core utilities...

## Documentation
All source code files are in the src/ directory. When Local binaries are generated, you can find then in bin/.

### About 
* Author: João Pedro M.V (DoutorJP).
* Date: 03/19/22
* License: GPL 2.0
