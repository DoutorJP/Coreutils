all:
	gcc -Wall src/cat.c -o bin/cat
	gcc -Wall src/echo.c -o bin/echo
	gcc -Wall src/strings.c -o bin/strings
clean: 
	rm -fr bin/*
